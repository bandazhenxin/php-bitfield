<?php
namespace bdhert\PhpBitfield;

/**
 * 位域串结构
 * Interface BitString
 * @package bdhert\PhpBitfield
 */
interface BitString {
    public function formatCheck(): bool;
}