<?php
namespace bdhert\PhpBitfield;

/**
 * 位图结构
 * Interface Map
 * @warn 由于本包强调的是压缩，位图长度不宜过长，所以位图索引的意义自行定义不做哈希映射。
 * @package bdhert\PhpBitfield
 */
interface Map {
    public function setbit(int $index): bool;
    public function delbit(int $index): bool;
    public function getbit(int $index): ?int;
    public function bitcount(): int;
    public function count(): int;
}