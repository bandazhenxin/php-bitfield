<?php
namespace bdhert\PhpBitfield;

/**
 * 关系集合结构
 * Interface BitCollect
 * @method Object head()
 * @method Collect initialize(string $binary)
 * @method string string()
 * @package bdhert\PhpBitfield
 */
interface Collect {
    public function get(): array;
    public function count(): int;
    public function first(): ?array;
    public function getbit(int $index): ?int;
    public function setbit(int $index);
    public function delbit(int $index);
    public function bitcount(): int;
    public function increment(int $n, int $number = 1);
    public function decrement(int $n, int $number = 1);
    public function update(array $saves);
    public function insert(array $save);
    public function limit(int $page, int $limit = 0): Collect;
    public function where($conditions = [], $operation = NULL, $value = NULL): Collect;
    public function whereReset($conditions = [], $operation = NULL, $value = NULL): Collect;
    public function desc(): Collect;
    public function asc(): Collect;
}