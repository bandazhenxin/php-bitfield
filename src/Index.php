<?php
namespace bdhert\PhpBitfield;

/**
 * Index结构
 * 顺序集合，可看作一个字段的集合结构 目的是最大化节省空间
 * Interface Index
 * @package bdhert\PhpBitfield
 */
interface Index {
    public function get(): array;
    public function count(): int;
    public function find(int $index): ?int;
    public function index(int $value): ?int; // 只返回第一个值的index
    public function insert(int $value): string;
    public function limit(int $page, int $limit = 0): Index;
}