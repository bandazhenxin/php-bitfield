# php-bitfield

## 说明

#### 介绍
位域，即使用二进制按照行列的规整结构存储信息的字符串，类比于redis的bitfield结构。本包实现压缩后的位域字符串基本的的增删改查操作。

#### 架构
无任何包依赖

#### 思想
* 使用场景：位域用于压缩存储信息使用，除了这个一般的目的我们可以将位域结合mysql字段进行设计，可以很有效得达到节约空间、高效查询的目的。比如可以简化一对多模型、简化记录表、将静态信息压缩存储等。特别的，在业务中我们经常需要根据数据的记录、状态的真或假来走if条件句，如果我们的数据量很大这时候频繁的IO查询是非常吃性能的，而使用位域的方式存储可以节约几百倍的查询量。
* 位域结构：我使用包头 + 数据的方式。包头占据固定位长105bit，存储片索引、字段数、各字段位数、统计数据等信息。数据由N个数据记录单元组成，这一点跟Mysql一样，每个单元定义固定的几个字段。可以理解为只使用一段字符串把一张表的数据存储下来。建议每段位域串长度不超过mysql的字段长度上限，那么一个位域串大概可以记录一两千条左右的数据。


## 使用

#### 安装

1.  composer require bdhert/php-bitfield
2.  开发版：composer require bdhert/php-bitfield:"dev-master"
3.  基础版：composer require bdhert/php-bitfield:"^0.1"

#### 使用

```php

<?php
use bdhert\PhpBitfield\BitANLS;
use bdhert\PhpBitfield\exception\BitFieldException;

try {
    $BA = new BitANLS($str);
    $BA->insert([[0 => 1, 2 => 1], [0 => 1, 1 => 1], [0 => 1, 1 => 1, 2 => 0]]);
    $BA->where(2, 0)->increment(2);
    return $BA->limit(1)->get();
} catch (BitFieldException $e) {
    throw new SignException($e->getMessage(), 400);
}

```

